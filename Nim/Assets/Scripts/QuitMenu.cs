﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitMenu : MonoBehaviour
{
	/// <summary>
	/// Button call
	/// Load up the main menu scene (MainMenu)
	/// </summary>
	public void ReturnToMain()
	{
		SceneManager.LoadScene("MainMenu");
	}

	/// <summary>
	/// Button call
	/// Quit application
	/// </summary>
	public void FullQuit()
	{
		Application.Quit();
	}
}
