﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Assets.Scripts;

public class SetupMenu : MonoBehaviour
{
	bool m_isReady = false;
	Difficulty.eDifficulty m_boardSize;

	/// <summary>
	/// Load up the main menu scene (MainMenu)
	/// </summary>
	public void ReturnToMain()
    {
        AI bot = GameObject.FindGameObjectWithTag("AI").GetComponent<AI>();
        Destroy(bot.gameObject);
        SceneManager.LoadScene("MainMenu");
	}

	/// <summary>
	/// Button call
	/// Change board size to small
	/// </summary>
	public void SelectSmall()
	{
		m_isReady = true;
		m_boardSize = Difficulty.eDifficulty.EASY;
	}

	/// <summary>
	/// Button call
	/// Change board size to medium
	/// </summary>
	public void SelectMedium()
	{
		m_isReady = true;
		m_boardSize = Difficulty.eDifficulty.MEDIUM;
	}

	/// <summary>
	/// Button call
	/// Change board size to large
	/// </summary>
	public void SelectLarge()
	{
		m_isReady = true;
		m_boardSize = Difficulty.eDifficulty.HARD;
	}

	/// <summary>
	/// Button call
	/// Loads up a scene dependent on the m_boardSize variable
	/// Loading will only happpen if a size has been selected
	/// </summary>
	public void PlayPlayer()
	{
		if(m_isReady)
        {
            AI bot = GameObject.FindGameObjectWithTag("AI").GetComponent<AI>();
            if (!bot.IsActve)
            {
                Destroy(bot.gameObject);
            }
            switch (m_boardSize)
			{
				case Difficulty.eDifficulty.EASY:
					SceneManager.LoadScene("GameSmall");
					break;
				case Difficulty.eDifficulty.MEDIUM:
					SceneManager.LoadScene("GameMedium");
					break;
				case Difficulty.eDifficulty.HARD:
					SceneManager.LoadScene("GameLarge");
					break;

			}
		}
	}

	/// <summary>
	/// Button call
	/// Calls the AI init function, sets difficulty to easy
	/// Calls PlayPlayer to start game and load scene
	/// </summary>
	public void PlayEasy()
	{
		if (m_isReady)
		{
			AI bot = GameObject.FindGameObjectWithTag("AI").GetComponent<AI>();
			bot.Init(Difficulty.eDifficulty.EASY);
			PlayPlayer();
		}
	}

	/// <summary>
	/// Button call
	/// Calls the AI init function, sets difficulty to medium
	/// Calls PlayPlayer to start game and load scene
	/// </summary>
	public void PlayMedium()
	{
		AI bot = GameObject.FindGameObjectWithTag("AI").GetComponent<AI>();
		bot.Init(Difficulty.eDifficulty.MEDIUM);
		PlayPlayer();
	}

	/// <summary>
	/// Button call
	/// Calls AI init function, sets difficulty to hard
	/// Calls PlayPlayer to start game and load scene
	/// </summary>
	public void PlayHard()
	{
		AI bot = GameObject.FindGameObjectWithTag("AI").GetComponent<AI>();
		bot.Init(Difficulty.eDifficulty.HARD);
		PlayPlayer();
	}
}
