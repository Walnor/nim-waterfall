﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
	/// <summary>
	/// Public getter for if the AI is active
	/// </summary>
	public bool IsActve { get { return m_isActive; } }
	bool m_isActive = false;

	Difficulty.eDifficulty m_level;

	public void Start()
	{
		DontDestroyOnLoad(this);
	}

	/// <summary>
	/// Setup the AI, activate the AI
	/// </summary>
	/// <param name="level"> the difficulty level of the AI </param>
	public void Init(Difficulty.eDifficulty level)
	{
		gameObject.tag = "AI";
		m_level = level;
		m_isActive = true;
	}

	/// <summary>
	/// Gets the AI's turn based on the heaps inputted
	/// </summary>
	/// <param name="heaps"> the selection of heaps to base the AI's turn off of </param>
	/// <returns> array of ints, size 2, first is heap index and second is amount from heap </returns>
	public int[] TakeTurn(Heap[] heaps)
	{
		int[] dou = new int[] { -1, 0 };
		int choice = -1;

		if (m_level == Difficulty.eDifficulty.MEDIUM)
		{
			if (Random.value > 0.5) choice = 0;
			else choice = 1;
		}

		if (m_level == Difficulty.eDifficulty.EASY || choice == 0)
		{
			bool valid = false;
			while (!valid)
			{
                //Debug.Log(heaps.Length);
				int selection = Random.Range(0, heaps.Length);
				if (heaps[selection].m_Amount > 0)
				{
					dou[0] = selection;
					dou[1] = Random.Range(1, heaps[selection].m_Amount + 1);
					valid = true;
				}
			}
		}
		else if (m_level == Difficulty.eDifficulty.HARD || choice == 1)
        {
            bool ones = false;
            bool twos = false;
            bool fours = false;
            
            List<int> onesForHeap = new List<int>();
            List<int> twosForHeap = new List<int>();
            List<int> foursForHeap = new List<int>();

            int NumOfStacksLeft = 0;
            foreach (Heap h in heaps)
            {
                if (h.m_Amount > 0)
                    NumOfStacksLeft++;
            }

            if (NumOfStacksLeft == 1)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount > 0)
                    {
                        dou[0] = i;
                        dou[1] = heaps[i].m_Amount - 1;

                        if (dou[1] < 1)
                            dou[1] = 1;

                        return dou;
                    }
                }
            }

            for (int i = 0; i < heaps.Length; i++)
            {
                onesForHeap.Add(0);
                twosForHeap.Add(0);
                foursForHeap.Add(0);

                int amount = heaps[i].m_Amount;

                while (amount >= 4)
                {
                    amount -= 4;
                    fours = !fours;
                    foursForHeap[i]++;
                }
                while (amount >= 2)
                {
                    amount -= 2;
                    twos = !twos;
                    twosForHeap[i]++;
                }
                while (amount >= 1)
                {
                    amount -= 1;
                    ones = !ones;
                    onesForHeap[i]++;
                }
            }

            int amountToTake = 0;
            if (ones) amountToTake += 1;
            if (twos) amountToTake += 2;
            if (fours) amountToTake += 4;

            if (amountToTake == 0)
                amountToTake = 1;

            if (amountToTake == 1)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 1;
                    }
                }
            }
            else if (amountToTake == 2)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 2 && twosForHeap[i] % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 2;
                    }
                }
            }
            else if (amountToTake == 4)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 4 && foursForHeap[i] % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 4;
                    }
                }
            }
            else if (amountToTake == 3)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 3 && onesForHeap[i] % 2 == 1 && twosForHeap[i] % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 3;
                    }
                }
            }
            else if (amountToTake == 5)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 5 && onesForHeap[i] % 2 == 1 && foursForHeap[i] % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 5;
                    }
                }
            }
            else if (amountToTake == 6)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 6 && twosForHeap[i] % 2 == 1 && foursForHeap[i] % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 6;
                    }
                }
            }
            else if (amountToTake == 7)
            {
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 7 && onesForHeap[i] % 2 == 1 && twosForHeap[i] % 2 == 1 && foursForHeap[i] % 2 == 1)
                    {
                        dou[0] = i;
                        dou[1] = 7;
                    }
                }
            }

            if (dou[1] == 0)
                for (int i = 0; i < heaps.Length; i++)
                {
                    if (heaps[i].m_Amount >= 1)
                    {
                        dou[0] = i;
                        dou[1] = 1;
                    }
                }

            int heapsLeft = 0;
            bool heapHasOne = false;
            bool TwoHeapsWithOne = false;

            foreach (Heap h in heaps)
            {
                if (h.m_Amount > 0)
                    heapsLeft++;

                if (h.m_Amount == 1)
                {
                    if (heapHasOne)
                    {
                        TwoHeapsWithOne = true;
                    }
                    heapHasOne = true;
                }
            }


            if (heapsLeft == 2)
            {
                if (TwoHeapsWithOne)
                {
                    for (int i = 0; i < heaps.Length; i++)
                    {
                        if (heaps[i].m_Amount == 1)
                        {
                            dou[0] = i;
                            dou[1] = 1;
                        }
                    }
                }
                else if (heapHasOne)
                {
                    for (int i = 0; i < heaps.Length; i++)
                    {
                        if (heaps[i].m_Amount > 1)
                        {
                            dou[0] = i;
                            dou[1] = heaps[i].m_Amount;
                        }
                    }
                }
            }
        }

		return dou;
	}
}
