﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heap : MonoBehaviour {

	/// <summary>
	/// Variable int for amount in a heap
	/// </summary>
    public int m_Amount = 3;

	/// <summary>
	/// Set the size of the heap
	/// </summary>
	/// <param name="amount"> the desired size of the heap </param>
    public void init(int amount)
    {
        m_Amount = amount;
    }

	/// <summary>
	/// Decrease the heap's amount by a specified amount
	/// </summary>
	/// <param name="amount"> the amount to take from the heap </param>
    public void Decrease(int amount)
    {
        m_Amount -= amount;
    }
}
