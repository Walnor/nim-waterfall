﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour {
    

    [SerializeField] Heap[] m_heaps;
    public Text[] m_heapsDisplay;

    public Image m_heapPointer;

	AI m_bot;

    int m_SelectedHeap = 0;
    int m_SelectedAmount = 0;

    public Text m_InvalidMoveText;

    private bool m_playerOne = true;
    private bool m_GameEnd = false;

    [SerializeField] Text m_SelectedAmountText;
    [SerializeField] Text m_CurrentPlayerTurnText;


	/// <summary>
	/// Initalizing function
	/// Finds the AI object, sets m_bot to it
	/// </summary>
	void Start ()
	{ /* This isn't needed until I start to work on the AI */
        GameObject obj = GameObject.FindGameObjectWithTag("AI");
        if(obj != null)
            m_bot = obj.GetComponent<AI>();

        if (m_bot != null)
        {   //AI Easy Test
            //m_bot.Init(Assets.Scripts.Difficulty.eDifficulty.EASY);
            //initialize AI for game

            m_CurrentPlayerTurnText.text = "Player's Turn!";
        }
	}
	
	/// <summary>
	/// Update function, called once per frame
	/// Leave to main menu after input if game is over
	/// Handles player inputs
	/// </summary>
	void Update ()
    {
        if (m_GameEnd)
        {
            if (Input.anyKeyDown)
            {   //return to the main menu
                ReturnToMenu();
            }
            return;
        }

        //handle game key input;
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            StackSelect(m_SelectedHeap - 1);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            StackSelect(m_SelectedHeap + 1);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            DecreaseAmount();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            IncreaseAmount();
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            ConfirmTurn();
        }
    }

	/// <summary>
	/// Updates the AI variable if m_bot doesn't equal null
	/// </summary>
    void UpdateAI()
    {
        //Call AI turn if exists
        int[] AISelection = m_bot.TakeTurn(m_heaps);

        StackSelect(AISelection[0]);

        m_SelectedAmount = AISelection[1];
        m_SelectedAmountText.text = m_SelectedAmount.ToString();        

        //Update game board
        m_heaps[m_SelectedHeap].Decrease(m_SelectedAmount);
        m_heapsDisplay[m_SelectedHeap].text = "";
        for (int i = 0; i < m_heaps[m_SelectedHeap].m_Amount; i++)
        {
            m_heapsDisplay[m_SelectedHeap].text += "|  ";
        }
        m_heapsDisplay[m_SelectedHeap].text = m_heapsDisplay[m_SelectedHeap].text.Trim();
        m_SelectedAmount = 1;
        m_SelectedAmountText.text = m_SelectedAmount.ToString();

        //If last item from last heap has been taken, the player that took it lost
        int totalItemsLeft = 0;
        foreach (Heap h in m_heaps)
        {
            totalItemsLeft += h.m_Amount;
        }

        //if Call GameEnd() and return
        if (totalItemsLeft == 0)
        {
            GameEnd();
            return;
        }
        m_playerOne = !m_playerOne;
    }

	/// <summary>
	/// Button call
	/// Sets selected heap based on the index
	/// </summary>
	/// <param name="index"> the desired heap to be selected </param>
    public void StackSelect(int index)
    {
        if (index >= m_heaps.Length)
            return;

        if (index < 0)
            return;

        m_SelectedHeap = index;

        Text heapText = m_heapsDisplay[index];

        Vector3 newPos = m_heapPointer.transform.position;

        newPos.y = heapText.transform.position.y;

        m_heapPointer.transform.position = newPos;
    }

	/// <summary>
	/// Button call
	/// Increases the amount to take froma a heap
	/// Stops increasing if greater than amount in heap
	/// </summary>
    public void IncreaseAmount()
    {
        m_SelectedAmount++;

        if (m_SelectedAmount > m_heaps[m_SelectedHeap].m_Amount)
            m_SelectedAmount = m_heaps[m_SelectedHeap].m_Amount;

        m_SelectedAmountText.text = m_SelectedAmount.ToString();
    }

	/// <summary>
	/// Button call
	/// Decreases the amount to take from a heap
	/// Stops decreasing if less than 0
	/// </summary>
    public void DecreaseAmount()
    {
        m_SelectedAmount--;

        if (m_SelectedAmount < 0)
            m_SelectedAmount = 0;

        m_SelectedAmountText.text = m_SelectedAmount.ToString();
    }

	/// <summary>
	/// Button call
	/// Checks if selected heap and amount is valid
	/// </summary>
    public void ConfirmTurn()
    {
        Heap selectedH = m_heaps[m_SelectedHeap];

        //Check if selected stack is valid
        if (selectedH != null && selectedH.m_Amount > 0)
        {
            //Check is selected amount is valid
            if (selectedH.m_Amount >= m_SelectedAmount && m_SelectedAmount > 0)
            {
                //if both valid call UpdateGame()
                UpdateGame();
            }
            else
            {
                //else inform player of invalid input/s
                m_InvalidMoveText.text = "The Selected Amount\nIs invalid";
            }
        }
        else
        {
            //else inform player of invalid input/s
            m_InvalidMoveText.text = "The Selected Heap\nIs invalid";
        }

    }

	/// <summary>
	/// Updates the game board
	/// Updates heaps and their amounts
	/// Indexes players
	/// </summary>
    public void UpdateGame()
    {
        //Update game board
        m_heaps[m_SelectedHeap].Decrease(m_SelectedAmount);
        m_heapsDisplay[m_SelectedHeap].text = "";
        for (int i = 0; i < m_heaps[m_SelectedHeap].m_Amount; i++)
        {
            m_heapsDisplay[m_SelectedHeap].text += "|  ";
        }
        m_heapsDisplay[m_SelectedHeap].text = m_heapsDisplay[m_SelectedHeap].text.Trim();
        m_SelectedAmount = 1;
        m_SelectedAmountText.text = m_SelectedAmount.ToString();

        //If last item from last heap has been taken, the player that took it lost
        int totalItemsLeft = 0;
        foreach (Heap h in m_heaps)
        {
            totalItemsLeft += h.m_Amount;
        }
        

        //if Call GameEnd() and return
        if (totalItemsLeft == 0)
        {
            GameEnd();
        }
        else
        {
            //else inverse m_playerOne
            m_playerOne = !m_playerOne;

            if (m_playerOne)
            {
                m_CurrentPlayerTurnText.text = "Player One's Turn";
            }
            else
			{                
				if (m_bot != null)
				{
                    //If AI call UpdateAI for AI's turn
					UpdateAI();
				}
				else
                {
                    m_CurrentPlayerTurnText.text = "Player Two's Turn";
				}
            }
        }
    }

	/// <summary>
	/// Displays winners and losers
	/// Ends game
	/// </summary>
    void GameEnd()
    {

        if (m_bot != null)
        {
            if (m_playerOne)
            {
                m_CurrentPlayerTurnText.text = "The Player has lost\nThe Computer Won";
            }
            else
            {
                m_CurrentPlayerTurnText.text = "The Computer lost\nThe Player has Won";
            }
        }
        else
        {
            if (m_playerOne)
            {
                m_CurrentPlayerTurnText.text = "Player One has lost\nPlayer Two has Won";
            }
            else
            {
                m_CurrentPlayerTurnText.text = "Player Two has lost\nPlayer One has Won";
            }
        }
        //Declare winner
        m_GameEnd = true;
    }

	/// <summary>
	/// Destorys the AI
	/// Loads up the main menu scene (MainMenu)
	/// </summary>
    void ReturnToMenu()
    {
        if (m_bot != null)
        {
            Destroy(m_bot.gameObject);
        }
		SceneManager.LoadScene("MainMenu");
	}
}
