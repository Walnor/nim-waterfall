﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	/// <summary>
	/// Button call
	/// Load up the quiting scene (QuitMenu)
	/// </summary>
	public void GoToQuit()
	{
		SceneManager.LoadScene("QuitMenu");
	}

	/// <summary>
	/// Button call
	/// Load up the rules scene (RulesMenu)
	/// </summary>
	public void GoToRules()
	{
		SceneManager.LoadScene("RulesMenu");
	}

	/// <summary>
	/// Button call
	/// Load up the setup scene (SetupMenu)
	/// </summary>
	public void GoToSetup()
	{
		SceneManager.LoadScene("SetupMenu");
	}
}
